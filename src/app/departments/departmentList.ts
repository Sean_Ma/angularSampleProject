import { Department } from './department';

export const DEPARTMENTS: Department[] = [
  { id: 5101, name: 'IT'},
  { id: 5102, name: 'HR'},
  { id: 5103, name: 'Accounting'},
  { id: 5104, name: 'Environment'}
];
