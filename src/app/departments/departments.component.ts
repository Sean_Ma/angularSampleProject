import {Component, Input, OnInit} from '@angular/core';
import { Department } from './department';
import { DEPARTMENTS } from './departmentList';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  departments: Department[] = DEPARTMENTS;
  selectedDepartment: Department;

  constructor() { }

  ngOnInit() {
  }

  onSelect(department: Department): void {
    this.selectedDepartment = department;
  }

}
