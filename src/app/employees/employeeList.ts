import { Employee } from './employee';

export const EMPLOYEES: Employee[] = [
  { id: 11, name: 'Sean', departmentID: 5101, rank: 'Chairman'},
  { id: 12, name: 'Andy', departmentID: 5104, rank: 'Managing Director'},
  { id: 13, name: 'Cindy', departmentID: 5103, rank: 'Manager'},
  { id: 14, name: 'Anne', departmentID: 5102, rank: 'General Manager'},
  { id: 15, name: 'Clara', departmentID: 5103, rank: 'Chief'},
  { id: 16, name: 'Bonnie', departmentID: 5101, rank: 'Assistant Manager'},
  { id: 17, name: 'Jin', departmentID: 5102, rank: 'Deputy General Manager'},
  { id: 18, name: 'Andrew', departmentID: 5103, rank: 'Intern'},
  { id: 19, name: 'Nicolas', departmentID: 5103, rank: 'Staff'},
  { id: 20, name: 'Gary', departmentID: 5104, rank: 'Manager'},
];
