export class Employee {
  id: number;
  name: string;
  departmentID: number;
  rank: string;
}
