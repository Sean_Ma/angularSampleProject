import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EMPLOYEES } from './employeeList';
import { Department } from '../departments/department';
import { DEPARTMENTS } from '../departments/departmentList';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees: Employee[] = EMPLOYEES;
  rankList: string[] = ['Chairman', 'Vice Chairman', 'President', 'Senior Executive Vice President', 'Senior Managing Director',
  'Managing Director', 'Department Manager', 'Deputy General Manager', 'General Manager', 'Manager', 'Assistant Manager',
  'Chief', 'Assistant Manager', 'Staff', 'Intern'];
  departments: Department[] = DEPARTMENTS;
  selectedEmployee: Employee;

  constructor() { }

  ngOnInit() {
  }

  onSelect(employee: Employee): void {
    this.selectedEmployee = employee;
  }

  updateEmployee(newDeptID: string, newRank: string): void {
    this.selectedEmployee.departmentID = +newDeptID;
    this.selectedEmployee.rank = newRank;
  }

}
